import unittest
import ipaddress
import sqlite3


def test_rule(ip_src, ip_dst, proto):
    ip_src = int(ipaddress.IPv4Address(ip_src))
    ip_dst = int(ipaddress.IPv4Address(ip_dst))

    if not proto:
        sql = "select * from rules where ip_src = {} & mask_src and ip_dst = {} & mask_dst and proto is NULL limit 1 ;".format(
            ip_src, ip_dst)
    else:
        sql = "select * from rules where ip_src = {} & mask_src and ip_dst = {} & mask_dst and proto == {} limit 1; ".format(
            ip_src, ip_dst, proto)

    c = db.cursor()
    c.execute(sql)

    if c.fetchone():
        return True

    return False


db = sqlite3.connect('nowa')


def clean_table_in_sqlite_db():
    db.execute("DELETE FROM rules;")


def prepare_rule_in_sqlite_db(params):
    ip_src = int(ipaddress.IPv4Address(params[0]))
    mask_src = int(ipaddress.IPv4Address(params[1]))
    proto = params[2]
    ip_dst = int(ipaddress.IPv4Address(params[3]))
    mask_dst = int(ipaddress.IPv4Address(params[4]))

    sql = "INSERT INTO rules (ip_src, mask_src, proto, ip_dst, mask_dst) VALUES ( {}, {}, {}, {}, {});".format(
        ip_src, mask_src, proto, ip_dst, mask_dst)
    db.execute(sql)
    db.commit()


class TestIpMatching(unittest.TestCase):
    def test_1(self):
        params = ["10.10.0.0", "255.255.0.0", 1, "10.10.0.0", "255.255.0.0"]
        clean_table_in_sqlite_db()
        prepare_rule_in_sqlite_db(params)

        self.assertEqual(
            True,
            test_rule("10.10.5.4", "10.10.4.5", 1)
        )

    def test_2(self):
        params = ["10.10.0.0", "255.255.0.0", 1, "10.10.0.0", "255.255.0.0"]
        clean_table_in_sqlite_db()
        prepare_rule_in_sqlite_db(params)

        self.assertEqual(
            False,
            test_rule("10.11.5.4", "10.10.4.5", 1)
        )

    def test_3(self):
        params = ["0.10.0.0", "255.255.0.0", 1, "0.10.0.0", "255.255.0.0"]
        clean_table_in_sqlite_db()
        prepare_rule_in_sqlite_db(params)

        self.assertEqual(
            True,
            test_rule("0.10.5.4", "0.10.4.5", 1)
        )


unittest.main()
