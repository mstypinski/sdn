"""Docstring"""
#from ryu.base import app_manager
from flask import Flask, render_template, url_for, request, redirect
import json
import ipaddress
import sqlite3

app = Flask(__name__)
#def run_server():
#    hello()
#url_for('static', filename='chinese_firewall.jpg')


@app.route('/')
def index():
    return render_template('index.j2')

@app.route('/hello/')
def hello():
    return "Bello"

@app.route('/add/')
def add():
    return render_template('add.j2')

def dict_factory(cursor, row):
    d = {}
    for idx, col in enumerate(cursor.description):
        d[col[0]] = row[idx]
    return d


@app.route('/show/')
def show():
    with open('config.json') as cfg:
        config = json.load(cfg)
    FIELDS = config['fields']
    REVPROTO = config['revproto']
    keys = ['id', 'ip_src', 'mask_src', 'ip_dst', 'mask_dst', 'proto']

    sql = '''
    SELECT {}
    from rules
    ;
    '''.format(', '.join(keys))

    db = sqlite3.connect('../db.db')
    c = db.cursor()
    c.row_factory = dict_factory
    c.execute(sql)

    results_orig = c.fetchall()

    print('SQL_results: {}'.format(results_orig))

    results = list()
    for result in results_orig:
        row = dict()
        for key in keys:
            if key == 'id':
                row[key] = result[key]
            elif key == 'proto':
                row[key] = '{}({})'.format(
                                REVPROTO.get(str(result[key]), 'ALL'),
                                result[key])
            else:
                row[key] = str(ipaddress.IPv4Address(result[key]))
        results.append(row)

    return render_template('show.j2', fields=FIELDS, keys=keys, results=results)

@app.route('/save')
def save():
    with open('config.json') as cfg:
        config = json.load(cfg)
    FIELDS = config['fields']
    PROTOCOLS = config['protocols']

    #DEBUG
    print('dump request')
    for key in request.values.keys():
        print('{}: {}'.format(key, request.values[key]))
    print('done')

    formated = dict()
    for field in FIELDS:
        if field.get('format') == 'ip':
            formated[field['tag']] = int(ipaddress.IPv4Address(request.values[field['tag']]))
        elif field.get('tag') == 'proto':
            if request.values.get('proto'):
                formated[field['tag']] = PROTOCOLS[request.values[field['tag']]]
            else:
                formated['proto'] = 'NULL'
        else:
            formated[field['tag']] = request.values[field['tag']]

    #DEBUG
    print(formated)

    sql = '''
    INSERT INTO rules (
    ip_src,
    mask_src,
    ip_dst,
    mask_dst,
    proto
    )
    VALUES (
    {ip_src},
    {mask_src},
    {ip_dst},
    {mask_dst},
    {proto}
    );
    '''.format(**formated)

    db = sqlite3.connect('../db.db')
    c = db.cursor()
    c.execute(sql)
    db.commit()
    db.close()
    #return '{}'.format(formated)
    return redirect('/show')
