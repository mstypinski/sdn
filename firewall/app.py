import sqlite3
import time

from ryu.base import app_manager
from ryu.controller import ofp_event
from ryu.controller.handler import MAIN_DISPATCHER
from ryu.controller.handler import set_ev_cls
from ryu.lib.packet import packet, ethernet, ipv4
from ryu.ofproto import ofproto_v1_0, ofproto_v1_3
import ipaddress

from switch.switch import SimpleSwitch13


def test_rule(ip_src, ip_dst, proto):
    ip_src = int(ipaddress.IPv4Address(ip_src))
    ip_dst = int(ipaddress.IPv4Address(ip_dst))

    if proto not in [0x06, 0x11]:
        sql = "select id, ip_src, mask_src, ip_dst, mask_dst, proto from rules " \
              "where ip_src = {} & mask_src " \
              "and ip_dst = {} & mask_dst " \
              "and proto is NULL limit 1 ;".format(
            ip_src, ip_dst)
    else:
        sql = "select id, ip_src, mask_src, ip_dst, mask_dst, proto " \
              "from rules where ip_src = {} & mask_src " \
              "and ip_dst = {} & mask_dst " \
              "and (proto = {} or proto is null) limit 1;".format(
            ip_src, ip_dst, proto)

    db = sqlite3.connect('db.db')
    c = db.cursor()
    c.execute(sql)

    row = c.fetchone()
    db.close()
    return row


class Firewall(SimpleSwitch13):
    OFP_VERSIONS = [ofproto_v1_3.OFP_VERSION]

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    def start(self):
        print("{} starting".format(type(self)))
        return super().start()

    def stop(self):
        print("{} stopping".format(type(self)))
        super().stop()

    @set_ev_cls(ofp_event.EventOFPPacketIn, MAIN_DISPATCHER)
    def _packet_in_handler(self, ev):
        msg = ev.msg
        datapath = msg.datapath
        pkt = packet.Packet(msg.data)
        ip_pkts = pkt.get_protocols(ipv4.ipv4)
        if ip_pkts:
            ip_pkt: ipv4.ipv4 = ip_pkts[0]
            src_ip, dst_ip, proto = ip_pkt.src, ip_pkt.dst, ip_pkt.proto
            test = test_rule(src_ip, dst_ip, proto)
            if test:
                ofproto_used = datapath.ofproto
                parser = datapath.ofproto_parser
                _, ip_src, mask_src, ip_dst, mask_dst, proto_val = test
                match_args = {
                    'eth_type': 0x0800,
                    'ipv4_src': (str(ipaddress.IPv4Address(ip_src)), str(ipaddress.IPv4Address(mask_src))),
                    'ipv4_dst': (str(ipaddress.IPv4Address(ip_dst)), str(ipaddress.IPv4Address(mask_dst))),
                }
                if proto_val:
                    match_args['ip_proto'] = proto_val
                match = parser.OFPMatch(**match_args)
                actions = []
                self.add_flow(datapath, 3, match, actions)
                return

        super()._packet_in_handler(ev)