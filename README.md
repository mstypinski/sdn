SDN projekt
-------
# Środowisko

Żeby uruchomić projekt potrzebny jest python3, sqlite3, [mininet](http://mininet.org/download/) oraz ryu i flask.  
Zarówno ryu, jak i flask mogą być zainstalowane komendą `pip3 install -r requirements.txt` (najlepiej w virtualenvie).  
Pozostałe narzędzia należy zainstalować na własną rękę w systemie (prawdopodobnie przy użyciu menedżera pakietów danej dystrybucji).  

# Sieć

Żeby uruchomić sieć wirtualną wystarczy w katalogu `SSP` uruchomić komendę `ryu-manager ./firewall/app.py --verbose`.  
Następnie  trzeba uruchomić miniet'a `sudo mn --topo tree,1,2 --controller=remote,ip=127.0.0.1 --switch ovs,protocols=OpenFlow13`.  

# Panel kontrolny

Aby uruchomić interfejs webowy, należy w katalogu `ui` uruchomić skrypt `start.sh`.  
W celu wyczyszczenia bazy danych należy uruchomić skrypt `delete from rules where 1=1;` na bazie `db.db` przy pomocy `sqlite3`.  