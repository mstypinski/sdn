PRAGMA foreign_keys=OFF;
BEGIN TRANSACTION;
CREATE TABLE IF NOT EXISTS "rules"
(
	id integer
		constraint rules_pk
			primary key autoincrement,
	ip_src integer not null,
	mask_src integer not null,
	proto integer,
	ip_dst integer not null,
	mask_dst integer not null
);
DELETE FROM sqlite_sequence;
INSERT INTO sqlite_sequence VALUES('rules',0);
COMMIT;
